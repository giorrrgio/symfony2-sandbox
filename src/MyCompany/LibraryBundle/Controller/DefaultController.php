<?php

namespace MyCompany\LibraryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('MyCompanyLibraryBundle:Default:index.html.twig', array('name' => $name));
    }
}
